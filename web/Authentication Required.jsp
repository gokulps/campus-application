<%-- 
    Document   : Authentication Required
    Created on : 18 Aug, 2018, 9:11:32 PM
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./Scripts/demo.css"/>
        <title>Authentication Required</title>
    </head>
    <body>
        <h1>Enter your credentials!!!</h1><br/><br/>
        
        <form action ="processing" method="post">
        <label for="name">Enter Username</label>
        <input type="text" name="Field1" required=""/><br/><br/>
        <label for ="pass">Enter password</label>
        <input type="password" name="Field2" required=""/><br/><br/>
        <input type="submit"/>
        </form>
        <br/><br/><br/>
        <form method="post" action ="SignUp">
            <label for="newname">Enter Username</label>
            <input type="text" name="Field1" required=""/><br/><br/>
            <label for="newpass">Enter Password</label>
            <input type="password" name="Field2" required=""/><br/><br/>
            <label for="newpass2">Enter Password Again</label>
            <input type="password" name="Field3" required=""/><br/><br/>
            <input type="submit"/>
        </form>
    </body>
</html>
