/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CAMPS.Connect;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dell
 */
@WebServlet(name = "update", urlPatterns = {"/update"})
public class update extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            DBConnect conn = new DBConnect();
            
            HttpSession session1 = request.getSession();
            String n = (String)session1.getAttribute("U_NAME");
            if(session1.getAttribute("U_NAME")==null)
            {
                RequestDispatcher rd = request.getRequestDispatcher("Authentication Required.jsp");
                rd.forward(request,response);
            }
            else
            {
            try
            {
            conn.getConnection();
            String name  = request.getParameter("update_pass");
            mdjavahash md = new mdjavahash();
            String mypass = md.getHashPass(name);
            escapeSpecialChars objesc = new escapeSpecialChars();
            String ip = request.getHeader("X-FORWARDED-FOR");
            request.getHeader("VIA");
            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                 ipAddress = request.getRemoteAddr();
                }
            ip = ipAddress;
            Date dbnow = new Date();
            String now = dbnow.toString();
            String sql = "update campusdemo.demo set password = '"+mypass+"' where name = '"+objesc.escapeSpecialChar(n)+"'";
            String sql1 = "insert into campusdemo.update_log(user_name,newpass,ip,Time) values('"+objesc.escapeSpecialChar(n)+"','"+mypass+"','"+ip+"','"+now+"')";
            conn.update(sql);
            conn.update(sql1);
            out.write("Updation Successfull, Login Again!!!");
            RequestDispatcher rd = request.getRequestDispatcher("logout.jsp");
            rd.forward(request,response);
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
            
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
